# Mercadolibre Scraper

## Instalacion

Asegurarse de usar `python >= 3.7` y de tener scrapy instalado:

``` bash
pip install scrapy
```

## Spiders

### ml_find_categories

Encuentra las sub-categorias con sus links y la cantidad de articulos por categoria. Su salida es la entrada de la spider ml_catalog.

#### Argumentos

- `url`: url al /categorias de ese pais. Por ej: `https://www.mercadolibre.com.uy/categorias`. Indica de que pais se va a scrapear.

#### Salida

- `id_category`: id unico para cada categoria.
- `main_category`: Categoria principal.
- `sub_category`: Sub-categoria.
- `result_count`: Cantidad de resultado en esa sub-categoria.
- `url`: url a la sub-categoria.


#### Uso

``` bash
scrapy crawl ml_find_category -o categories_uy.csv -L INFO -a url=https://www.mercadolibre.com.uy/categorias
```

### ml_catalog

Guarda info de los articulos de cada subcategoria. Saltea los articulos promocionados (porque no muestan en link de forma directa) y scrapea como maximo 40 paginas de esa categoria.

#### Argumentos

- `categories_csv`: csv con los links a las categorias a scrapear.

#### Salida

- `id_category`: id de la categoria (el que esta en `categories_csv`)
- `price`: precio del articulo.
- `price_old`: Si tiene descuento, precio sin descuento.
- `title`: Titulo del articulo
- `url`: Url del articulo.

#### Uso

``` bash
scrapy crawl ml_catalog -o items_ar.csv -L INFO --logfile ml_catalog_ar.log -a categories_csv=categories_ar.csv
```
