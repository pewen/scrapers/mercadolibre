import scrapy
from mercadolibre_scrapper.items import CategoryItem

# TODO: mirar que la cantidad de requets sea similar a la cantidad de items
# menos el requests inicial, menos los errores (adultos en este caso).


class MlFindCategorySpider(scrapy.Spider):
    name = "ml_find_category"

    def __init__(self, url='https://www.mercadolibre.com.ar/categorias', *args, **kwargs):
        super(MlFindCategorySpider, self).__init__(*args, **kwargs)
        allowed_domains = url
        for s in ['https://', 'http://', 'www.']:
            allowed_domains = allowed_domains.replace(s, '')
        allowed_domains = allowed_domains.rsplit('/', maxsplit=1)[0]
        self.start_urls = [url]

    def parse(self, response):
        categories = response.xpath("//div[@class='categories__container']")
        for cat in categories:
            for cat_link in cat.xpath("ul/li/a/@href").getall():
                yield scrapy.Request(cat_link, callback=self.parse_category)

    def parse_category(self, response):
        item = CategoryItem()
        item['main_category'] = response.xpath("//div[@class='breadcrumb']//a/@title").get()
        item['sub_category'] = response.xpath("//div[@class='breadcrumb']/h1/text()").get()
        item['url'] = response.url
        item['result_count'] = response.xpath("//div[@class='quantity-results']/text()").get('').strip().split()[0].replace('.', '')
        yield item
