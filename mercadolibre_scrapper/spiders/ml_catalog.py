import csv
from urllib.parse import urljoin

import scrapy
from mercadolibre_scrapper.items import CatalogItem

# def parse_stars(raw_stars):
#     stars_values = {"star star-icon-full": 1,
#                     "star star-icon-half": 0.5,
#                     "star ": 0}

#     return sum(stars_values[star] for star in raw_stars)


# def get_id_from_url(url):
#     temp_url = url.rsplit("/", maxsplit=1)[1]
#     if url.startswith("https://www.mercadolibre.com.ar/"):
#         return temp_url
#     else:
#         return ''.join(temp_url.split('-', maxsplit=2)[:-1])

class MlCatalogSpider(scrapy.Spider):
    name = 'ml_catalog'

    def start_requests(self):
        with open(self.categories_csv) as f:
            reader = csv.DictReader(f)
            for row in reader:
                yield scrapy.Request(urljoin(row['url'], "_DisplayType_LF"),
                                     callback=self.parse,
                                     cb_kwargs={'id_category': row['id_category']})

    def parse(self, response, id_category):

        products = response.xpath("//ol[@id='searchResults']/li")

        for p in products:
            item = CatalogItem()
            p_url = p.xpath(".//a[@class='item__info-title']/@href").get("").split("?", maxsplit=1)[0].split("#", maxsplit=1)[0]

            # Need follow the link/redirection to get
            # the real link for promotional products.
            if p_url.startswith("https://click1"):  # discard for now
                continue

            item["url"] = p_url
            item["id_category"] = id_category
            item["title"] = p.xpath(".//span[@class='main-title']/text()").get().strip()
            prices = p.xpath(".//div[@class='price__container']/div")
            item["price"] = prices.xpath(".//span[@class='price__fraction']/text()").get()
            item["price_old"] = prices.xpath("./span/del/text()").get("")
            yield item

        next_page_url = response.xpath("//li[@class='andes-pagination__button andes-pagination__button--next ']/a/@href").get('')
        if next_page_url:
            yield scrapy.Request(next_page_url,
                                 callback=self.parse,
                                 cb_kwargs={'id_category': id_category})
