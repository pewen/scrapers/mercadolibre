# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

from mercadolibre_scrapper.items import CategoryItem


class CategoryEnumeratePipeline(object):
    def __init__(self):
        self.id_category_count = 0

    def process_item(self, item, spider):
        if isinstance(item, CategoryItem):
            item['id_category'] = self.id_category_count
            self.id_category_count += 1

        return item
