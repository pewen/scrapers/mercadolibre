# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy

class CategoryItem(scrapy.Item):
    url = scrapy.Field()
    id_category = scrapy.Field()
    main_category = scrapy.Field()
    sub_category = scrapy.Field()
    result_count = scrapy.Field()

class CatalogItem(scrapy.Item):
    title = scrapy.Field()
    price = scrapy.Field()
    price_old = scrapy.Field()
    url = scrapy.Field()
    id_category = scrapy.Field()
